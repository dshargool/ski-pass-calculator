import numpy as np


def main():
    mc_matrix = create_mc_cost_matrix(9)
    # print(spent_matrix)
    # print(np.min(spent_matrix))
    # print(np.argmin(spent_matrix))

    print("------------------------------------------------")
    plus_matrix = create_plus_cost_matrix(9)
    # print(spent_matrix)
    # print(np.min(spent_matrix))
    # print(np.argmin(spent_matrix))

    diff_matrix = plus_matrix - mc_matrix
    diff_matrix = diff_matrix.clip(min=0)
    print(diff_matrix)
    print(np.min(diff_matrix))
    print(np.argmin(diff_matrix))


def create_mc_cost_matrix(num_days):
    mc_cost = 590
    ll_day_cost = 129
    ssv_day_cost = 127
    revy_day_cost = 129

    cost_matrix = np.zeros((num_days, num_days, num_days))
    cost_matrix[0][0][0] = mc_cost
    for ll in range(0, num_days):
        for ssv in range(0, num_days):
            for revy in range(0, num_days):
                print("LL:" + str(ll))
                print("SSV:" + str(ssv))
                print("REVY:" + str(revy))
                if ssv + ll + revy > 0:
                    mc_cost_day = mc_cost / (ll + ssv + revy)
                    cost_matrix[ll][ssv][revy] = (
                        price_mc_num_days(ll_day_cost, mc_cost_day, ll)
                        + price_mc_num_days(ssv_day_cost, mc_cost_day, ssv)
                        + price_mc_num_days(revy_day_cost, mc_cost_day, revy)
                    )
                print(cost_matrix[ll][ssv][revy])

    np.set_printoptions(precision=2)
    return cost_matrix


def create_plus_cost_matrix(num_days):
    ll_day_cost = 129
    ssv_day_cost = 127
    ll_plus_cost = 125
    ssv_plus_cost = 100

    cost_matrix = np.zeros((num_days, num_days, num_days))
    cost_matrix[0][0][0] = ll_plus_cost + ssv_plus_cost
    for ll in range(0, num_days):
        for ssv in range(0, num_days):
            for revy in range(0, num_days):
                print("LL:" + str(ll))
                print("SSV:" + str(ssv))
                print("REVY:" + str(revy))
                if ssv + revy > 0:
                    plus_cost_day_ssv = ssv_plus_cost / (ssv + revy)
                else:
                    plus_cost_day_ssv = ssv_plus_cost
                if ll > 0:
                    plus_cost_day_ll = ll_plus_cost / ll
                else:
                    plus_cost_day_ll = ll_plus_cost

                cost_matrix[ll][ssv][revy] = price_ll_plus_num_days(
                    ll_day_cost, plus_cost_day_ll, ll
                ) + price_ssv_plus_num_days(ssv_day_cost, plus_cost_day_ssv, ssv + revy)
                print(cost_matrix[ll][ssv][revy])
    return cost_matrix


def price_mc_on_day(day_cost, pass_cost_per_day, resort_days):
    if resort_days <= 2:
        cost = pass_cost_per_day
    else:
        cost = pass_cost_per_day + day_cost * 0.5
    return cost


def price_mc_num_days(day_cost, pass_cost_per_day, resort_days):
    cost = 0
    if resort_days == 0:
        return pass_cost_per_day
    for i in range(1, resort_days + 1):
        cost += price_mc_on_day(day_cost, pass_cost_per_day, i)
    return cost


def price_ssv_plus_on_day(day_cost, pass_cost_per_day, resort_days):
    if resort_days == 1 or resort_days == 4 or resort_days == 7:
        cost = pass_cost_per_day
    else:
        cost = pass_cost_per_day + day_cost - 15
    return cost


def price_ssv_plus_num_days(day_cost, pass_cost_per_day, resort_days):
    cost = 0
    if resort_days == 0:
        return pass_cost_per_day
    for i in range(1, resort_days + 1):
        cost += price_ssv_plus_on_day(day_cost, pass_cost_per_day, i)
    return cost


def price_ll_plus_on_day(day_cost, pass_cost_per_day, resort_days):
    if resort_days == 1 or resort_days == 4 or resort_days == 7:
        cost = pass_cost_per_day
    else:
        cost = pass_cost_per_day + day_cost *0.8
    return cost


def price_ll_plus_num_days(day_cost, pass_cost_per_day, resort_days):
    cost = 0
    if resort_days == 0:
        return pass_cost_per_day
    for i in range(1, resort_days + 1):
        cost += price_ll_plus_on_day(day_cost, pass_cost_per_day, i)
    return cost


main()
